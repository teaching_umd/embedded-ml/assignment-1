# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:41:17 2021

@author: sshah389
"""


import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
i=0
ser = serial.Serial('COM7', 9600, timeout=1) ##change this to your COMport



y_test_pred_tflite=sio.loadmat('./tflite_pred.mat') #This is what was predicted in you python file (Google Colab)

data_test=sio.loadmat('./commands_test.mat') #Change this to point to your dataset
Input_test=data_test['Input_test']
x_values=np.reshape(Input_test,[Input_test.shape[0],Input_test.shape[1],1])
y_values=data_test['Label_test']
y_values= tf.keras.utils.to_categorical(y_values)



sample=401#You can change this to test out differen samples
M=0
N=16000

input_1=x_values[sample,0:N]
int_value=np.zeros([N])
out1=np.zeros([N])
ser.write(b'\x7f')
model_correct=ser.readline()
size=ser.readline()
dims_input1=ser.readline()
dims_input2=ser.readline()
dims_input3=ser.readline()
dims_input4=ser.readline()
for i in range(0,N):
    int_value[i]=int(((x_values[sample,i]+1)*127))
    int_value1=int(int_value[i])
    int_bytes= int_value1.to_bytes(1,'big')
    ser.write(int_bytes)
    out1[i] =  ser.readline()
pred1 =  ser.readline()
pred2 =  ser.readline()
ser.close()


pyplot.plot(input_1,'r*')#These are just to make sure the input is similar to what is recieved in the hardware
pyplot.plot(out1)
pyplot.show()

print('out1:',pred1)#These are from the hardware you can use it to compare this with the one from the python modeling.
print('out2:',pred2)


y_test_pred_tflite=y_test_pred_tflite['y_test_pred_tflite']

print('out1_tflite_pred:',y_test_pred_tflite[sample,0])
print('out2_tflite_pred:',y_test_pred_tflite[sample,1])

