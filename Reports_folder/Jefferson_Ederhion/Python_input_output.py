# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:41:17 2021

@author: sshah389
"""

import serial
import numpy as np
from matplotlib import pyplot 
import math
import scipy.io as sio
import tensorflow as tf
from sklearn.metrics import accuracy_score

i=0
ser = serial.Serial('COM5', 9600, timeout=1) ##change this to your COMport

y_test_pred_tflite=sio.loadmat('./tflite_pred.mat') #This is what was predicted in you python file (Google Colab)

data_test=sio.loadmat('./Dataset/commands_test.mat') #Change this to point to your dataset
Input_test=data_test['Input_test']
x_values=np.reshape(Input_test,[Input_test.shape[0],Input_test.shape[1],1])
y_values=data_test['Label_test']
y_values= tf.keras.utils.to_categorical(y_values)

# Initialize array to store hardware predictions
hardware_preds = np.zeros([402, 2])

for sample in range(402): # Predict for 402 records
    M=0
    N=16000

    input_1=x_values[sample,0:N]
    int_value=np.zeros([N])
    out1=np.zeros([N])
    ser.write(b'\x7f')
    model_correct=ser.readline()
    size=ser.readline()
    dims_input1=ser.readline()
    dims_input2=ser.readline()
    dims_input3=ser.readline()
    dims_input4=ser.readline()
    for i in range(0,N):
        int_value[i]=int(((x_values[sample,i]+1)*127))
        int_value1=int(int_value[i])
        int_bytes= int_value1.to_bytes(1,'big')
        ser.write(int_bytes)
        out1[i] =  ser.readline()
    pred1 =  ser.readline()
    pred2 =  ser.readline()

    # Store hardware predictions
    hardware_preds[sample, 0] = pred1
    hardware_preds[sample, 1] = pred2
    
    print(sample, hardware_preds[sample, 0],  hardware_preds[sample, 1])

ser.close()

# Calculate accuracy
accuracy = accuracy_score(np.argmax(y_values, axis=1), np.argmax(hardware_preds, axis=1))
print('\n\n===============================================================')
print('Accuracy of the hardware: ', accuracy)
